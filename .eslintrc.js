module.exports = {
  "env": {
      "browser": true,
      "es6": true,
      "node": true
  },
  "extends": ["standard", "standard-react"],
  "parserOptions": {
      "ecmaFeatures": {
          "experimentalObjectRestSpread": true,
          "jsx": true
      },
      "sourceType": "module"
  },
  "plugins": [
      "react"
  ],
  "rules": {
      "experimentalDecorators": [0],
      "react/jsx-no-bind": [0, {allowBind: true, allowFunctions: true, allowArrowFunctions: true}],
      "react/prop-types": [0],
      "no-global-assign": [0],
      "indent": [0],
      "quotes": [
          "error",
          "single"
      ],
      "semi": [
          "error",
          "never"
      ]
  }
};