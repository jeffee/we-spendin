import React from 'react'
import ReactDOM from 'react-dom'
import registerServiceWorker from './registerServiceWorker'

import App from './pages/index.js'
import NoMatch from './pages/noMatch.js'
import Add from './pages/add.js'
import Login from './pages/login.js'
import Appbar from './views/appbar.js'

import './style.css'

import { HashRouter as Router, Route, Switch } from 'react-router-dom'

ReactDOM.render((
  <Router>
    <Appbar>
      <Switch>
        <Route exact path='/' component={App} />
        <Route path='/login' component={Login} />
        <Route path='/add' component={Add} />
        <Route component={NoMatch} />
      </Switch>
    </Appbar>
  </Router>
), document.getElementById('root'))
registerServiceWorker()
