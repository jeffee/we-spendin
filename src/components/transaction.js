import React from 'react'
import { withStyles } from '@material-ui/core/styles'
// import classNames from 'classnames'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions'
import Typography from '@material-ui/core/Typography'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Button from '@material-ui/core/Button'
import Divider from '@material-ui/core/Divider'
// import axios from 'axios'
import { Input } from '@material-ui/core'
import axios from 'axios'

import SaveButton from './save'

const styles = theme => ({
  root: {
    width: '100%'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15)
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  },
  icon: {
    verticalAlign: 'bottom',
    height: 20,
    width: 20
  },
  details: {
    alignItems: 'center'
  },
  column: {
    flexBasis: '33.33%'
  },
  helper: {
    borderLeft: `2px solid ${theme.palette.divider}`,
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`
  },
  link: {
    color: theme.palette.primary.main,
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'underline'
    }
  },
  input: {
    margin: theme.spacing.unit
  }
})

class DetailedExpansionPanel extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      expanded: false,
      saving: false,
      done: false,
      peli: '',
      open: false,
      price: this.props.data.hinta,
      saveError: null
    }
  }

  save () {
    this.setState({
      saving: true
    })

    if (window.login === false) {
      this.setState({ saveError: 'red', saving: false })

      setTimeout(() => {
        this.setState({
          expanded: false,
          saving: false,
          done: false,
          saveError: null
        })
      }, 3000)
    } else {
      if (this.state.price !== this.props.data.hinta) {
        axios.patch('https://sleier.herokuapp.com/api/transactions', { price: this.state.price, id: this.props.data.id })
          .then(() => {
            this.setState({
              saving: false,
              done: true
            })

            setTimeout(() => {
              this.setState({
                expanded: false,
                saving: false,
                done: false
              })
            }, 500)

            window.reloadTransactions()
          })
          .catch(e => {
            console.log(e)
            this.setState({
              saveError: 'red',
              saving: false
            })

            window.alert('Serveri ei toimi :(')
            setTimeout(() => {
              this.setState({
                expanded: false,
                saving: false,
                done: false,
                saveError: null
              })
            }, 3000)
          })
      } else {
        this.setState({
          saving: false,
          done: true
        })

        setTimeout(() => {
          this.setState({
            expanded: false,
            saving: false,
            done: false
          })
        }, 500)
      }
    }
  }

  handleInput (e) {
    this.setState({
      price: e.target.value
    })
  }

  handleChange (event) {
    console.log(event)
    this.setState({ [event.target.name]: event.target.value })
  };

  handleClose () {
    this.setState({ open: false })
  };

  handleOpen () {
    this.setState({ open: true })
  };

  render () {
    const { props } = this

    const { classes } = props
    return (
      <div className={classes.root} style={{marginTop: '3px'}}>
        <ExpansionPanel expanded={this.state.expanded}>
          <ExpansionPanelSummary onClick={() => this.setState({ expanded: !this.state.expanded })} expandIcon={<ExpandMoreIcon />}>
            <div className={classes.column}>
              <Typography className={classes.heading}>{props.data.tuote}</Typography>
            </div>
            <div className={classes.column}>
              <Typography className={classes.secondaryHeading}>{props.data.hinta + '€'}</Typography>
            </div>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className={classes.details}>
            <div className={classes.column}>
              {this.props.data.nimi}
            </div>
            <div className={classes.column}>
              <Input
                type='number'
                onChange={this.handleInput.bind(this)}
                className={classes.input}
                value={this.state.price}
                inputProps={{
                  'aria-label': 'Hinta'
                }}
              />
            </div>
            {/* <div className={classNames(classes.column, classes.helper)}>
              <Typography variant='caption'>
                Select your destination of choice<br />
                <a href='#sub-labels-and-columns' className={classes.link}>
                  Learn more
                </a>
              </Typography>
            </div> */}
          </ExpansionPanelDetails>
          <Divider />
          <ExpansionPanelActions>
            <Button style={{borderRadius: '8px'}} size='small' onClick={() => { this.setState({ expanded: false }) }}>Peruuta</Button>
            <div onClick={() => this.save()}>
              <SaveButton saving={this.state.saving} error={this.state.saveError} done={this.state.done} />
            </div>
          </ExpansionPanelActions>
        </ExpansionPanel>
      </div>
    )
  }
}

export default withStyles(styles)(DetailedExpansionPanel)
