import React from 'react'
import Paper from '@material-ui/core/Paper'

export default class extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      nimi: ''
    }
  }

  render () {
    return (
      <center>
        <Paper key='error' style={{maxWidth: '450px', paddingBottom: '8px', marginTop: '18px', borderRadius: '16px', marginLeft: '5px', marginRight: '5px'}}>
          <img style={{maxWidth: '400px', width: '100%', height: 'auto'}} alt='Under Construction' src='https://cdn.dribbble.com/users/143127/screenshots/2472789/crane-gif.gif' />
          <h1>Under Construction</h1>
        </Paper>
      </center>
    )
  }
}
