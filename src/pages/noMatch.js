import React from 'react'
import Paper from '@material-ui/core/Paper'

export default () => (
  <center>
    <Paper key='error' style={{maxWidth: '450px', paddingBottom: '8px', marginTop: '18px', borderRadius: '16px'}}>
      <img style={{maxWidth: '400px', width: '100%', height: 'auto'}} alt='404' src='https://marvelapp.com/static/assets/images/global/bad-luck.gif' />
      <h1>Ei tainnu löytyy mitää</h1>
    </Paper>
  </center>
)
