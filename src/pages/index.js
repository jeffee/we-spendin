import React from 'react'
import axios from 'axios'
import Paper from '@material-ui/core/Paper'

import Transaction from '../components/transaction.js'

class Index extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      transactions: []
    }

    window.reloadTransactions = async () => {
      try {
        let transactions = await axios.get('https://sleier.herokuapp.com/api/transactions')
        this.setState({ transactions: transactions.data })
      } catch (error) {
        console.log('Request failed', error)
      }
    }
  }

  async componentWillMount () {
    if (window.transactions) return this.setState({ transactions: window.transactions })
    try {
      let transactions = await axios.get('https://sleier.herokuapp.com/api/transactions')
      this.setState({ transactions: transactions.data })
    } catch (error) {
      console.log('Request failed', error)
    }
  }

  render () {
    let { transactions } = this.state
    let transactionsEL = transactions.map((transaction, index) => {
      if (transaction.id === 'error') {
        return (
          <center key='error'>
            <Paper style={{maxWidth: '450px', paddingBottom: '8px', marginTop: '18px'}}>
              <img alt='404' style={{maxWidth: '400px', width: '100%', height: 'auto'}} src='https://marvelapp.com/static/assets/images/global/bad-luck.gif' />
              <h1>Ei tainnu löytyy mitää</h1>
            </Paper>
          </center>
        )
      } else {
        return (<Transaction data={transaction} key={transaction.id} />)
      }
    })

    return (
      <div>
        <div style={{marginLeft: '8px', marginRight: '8px', marginTop: '20px'}}>
          {transactionsEL}
        </div>
      </div>
    )
  }
}

export default Index
