import React from 'react'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import CircularProgress from '@material-ui/core/CircularProgress'
import purple from '@material-ui/core/colors/purple'
import Button from '@material-ui/core/Button'

class Index extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      code: '',
      loggingIn: false,
      loggedIn: window.login,
      errorMsg: ''
    }
  }

  handleChange (e) {
    if (Number.isInteger(Number(e.target.value))) return this.setState({ code: e.target.value })
  }

  login () {
    console.log('yoo', this)
    this.setState({ loggingIn: true, errorMsg: '' })
    if (!this.state.code) return this.setState({ loggingIn: false, errorMsg: 'Koodia ei ole!' })
    let code = this.state.code
    setTimeout(() => {
      if (Number(code) === 25565) {
        window.login = true
        window.localStorage.setItem('auth', true)
        this.setState({
          loggedIn: true,
          loggingIn: false
        })
        window.linkSuccess()
      } else {
        this.setState({
          loggedIn: false,
          loggingIn: false,
          errorMsg: 'Koodi on väärin'
        })
      }
    }, 1000)
  }

  render () {
    return (
      <center>
        <Paper style={{maxWidth: '450px', paddingBottom: '8px', marginTop: '18px', borderRadius: '16px'}}>
          { this.state.loggedIn ? (
            <div>
              <h1 style={{fontFamily: 'Google Sans', paddingTop: '20px'}}>Kirjauduit Sisään</h1>
            </div>
          ) : (
            <div>
              { this.state.loggingIn ? (
                <CircularProgress style={{ color: purple[500], marginTop: '10px' }} thickness={7} />
              ) : (
                <div>
                  <TextField
                    id='number'
                    label='Code'
                    value={this.state.code}
                    onChange={this.handleChange.bind(this)}
                    type='password'
                    inputMode='numeric'
                    InputLabelProps={{
                      shrink: true
                    }}
                    margin='normal'
                    style={{paddingBottom: '8px'}}
                  />
                  <Button style={{marginLeft: '8px', height: '15px', borderRadius: '8px', fontFamily: 'Google Sans'}} variant='outlined' color='primary' onClick={this.login.bind(this)}>
                  Kirjaudu
                  </Button>
                  { this.state.errorMsg && (
                    <h2 style={{fontFamily: 'Google Sans'}}>{this.state.errorMsg}</h2>
                  )}
                </div>
              )}
            </div>
          )}
        </Paper>
      </center>
    )
  }
}

export default Index
