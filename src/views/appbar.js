import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import List from '@material-ui/core/List'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import Hidden from '@material-ui/core/Hidden'
import { Divider, ListItem } from '@material-ui/core'
import { Menu as MenuIcon, DirectionsRun as Logout, Home, Add, VpnKey as Login } from '@material-ui/icons'

import { Link } from 'react-router-dom'

const drawerWidth = 240

const styles = theme => ({
  root: {
    zIndex: 1,
    position: 'relative',
    display: 'flex'
  },
  appBar: {
    position: 'absolute',
    marginLeft: drawerWidth,
    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${drawerWidth}px)`
    },
    backgroundColor: '#FFFFFF'
  },
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
    [theme.breakpoints.up('md')]: {
      position: 'relative'
    }
  },
  content: {
    flexGrow: 1
  }
})

class ResponsiveDrawer extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      mobileOpen: false,
      login: false
    }
  }

  authenticate () {
    return new Promise(resolve => setTimeout(resolve, 300))
  }

  componentDidMount () {
    this.authenticate().then(() => {
      const ele = document.getElementById('ipl-progress-indicator')
      if (ele) {
        // fade out
        ele.classList.add('available')
        setTimeout(() => {
          // remove from DOM
          ele.outerHTML = ''
        }, 2000)
      }
    })
  }

  componentWillMount () {
    if (window.localStorage.getItem('auth')) {
      this.setState({
        login: true
      })
      window.login = true
    } else {
      this.setState({
        login: false
      })
      window.login = false
    }

    window.linkSuccess = () => {
      this.setState({
        login: true
      })
    }

    window.noAuth = () => {
      this.setState({
        login: false
      })
    }
  }

  logout () {
    window.localStorage.clear()
    window.login = false
    window.noAuth()
  }

  handleDrawerToggle () {
    this.setState({ mobileOpen: !this.state.mobileOpen })
  }

  render () {
    const { classes, theme } = this.props

    const drawer = (
      <div>
        <center>
          <Typography style={{fontFamily: 'Google Sans', fontSize: '45px', marginRight: '10px', color: '#717171'}}>jeffe</Typography>
        </center>
        <Divider />
        <List style={{paddingRight: '10px'}}>

          { !this.state.login && (
            <React.Fragment>
              <ListItem onClick={this.handleDrawerToggle.bind(this)} component={Link} to='/' style={{borderRadius: '0px 50px 50px 0px', height: '33px'}} button>
                <Home style={{marginRight: '5px', opacity: 0.54}} />
                <Typography style={{fontFamily: 'Google Sans', marginRight: '10px', color: '#717171'}}>Koti</Typography>
              </ListItem>

              <ListItem onClick={this.handleDrawerToggle.bind(this)} component={Link} to='/login' style={{borderRadius: '0px 50px 50px 0px', height: '33px'}} button>
                <Login style={{marginRight: '5px', opacity: 0.54}} />
                <Typography style={{fontFamily: 'Google Sans', marginRight: '10px', color: '#717171'}}>Kirjaudu</Typography>
              </ListItem>
            </React.Fragment>
          )}

          { this.state.login && (
            <React.Fragment>
              <ListItem onClick={this.handleDrawerToggle.bind(this)} component={Link} to='/' style={{borderRadius: '0px 50px 50px 0px', height: '33px'}} button>
                <Home style={{marginRight: '5px', opacity: 0.54}} />
                <Typography style={{fontFamily: 'Google Sans', marginRight: '10px', color: '#717171'}}>Koti</Typography>
              </ListItem>

              <ListItem onClick={this.handleDrawerToggle.bind(this)} component={Link} to={'/add'} style={{borderRadius: '0px 50px 50px 0px', height: '33px'}} button>
                <Add style={{marginRight: '5px', opacity: 0.54}} />
                <Typography style={{fontFamily: 'Google Sans', marginRight: '10px', color: '#717171'}}>Lisää</Typography>
              </ListItem>
            </React.Fragment>
          )}
        </List>
        { this.state.login && (
          <div>
            <Divider />

            <List style={{paddingRight: '10px'}}>
              <ListItem button component={Link} to='/' onClick={this.logout.bind(this)} style={{borderRadius: '0px 50px 50px 0px'}} >
                <Logout style={{marginRight: '5px', opacity: 0.54}} />
                <Typography style={{fontFamily: 'Google Sans', marginRight: '10px', color: '#717171'}}>Kirjaudu Ulos</Typography>
              </ListItem>
            </List>
          </div>
        )}
      </div>
    )

    return (
      <div className={classes.root}>
        <AppBar position='sticky' className={classes.appBar}>
          <Toolbar>
            <IconButton
              aria-label='avaa sivupaneeli'
              onClick={this.handleDrawerToggle.bind(this)}
              className={classes.navIconHide}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant='title' color='inherit'>
              <Typography style={{fontFamily: 'Google Sans', marginRight: '10px', color: '#717171'}}>We</Typography>
              <Typography style={{fontFamily: 'Google Sans', color: '#999999'}}>Countin</Typography>
            </Typography>
          </Toolbar>
        </AppBar>
        <Hidden mdUp>
          <Drawer
            variant='temporary'
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={this.state.mobileOpen}
            onClose={this.handleDrawerToggle.bind(this)}
            classes={{
              paper: classes.drawerPaper
            }}
            ModalProps={{
              keepMounted: true // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden smDown implementation='css'>
          <Drawer
            variant='permanent'
            open
            classes={{
              paper: classes.drawerPaper
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {this.props.children}
        </main>
      </div>
    )
  }
}

export default withStyles(styles, { withTheme: true })(ResponsiveDrawer)
